#!/usr/bin/env sh

export JAVA=/usr/lib/jvm/java-1.6.0/bin/java
export PATH=$PATH:/usr/local/ESCJava205/
escj -Suggest -loop 3 Queue.java
escj -Suggest -loop 3 Set.java
escj -Suggest -loop 3 Test.java
