 /*
 ** Copyright (C) 2008 José Vander Meulen <jose.vandermeulen@uclouvain.be>
 ** Copyright (C) 2010 Charles Pecheur <charles.pecheur@uclouvain.be>
 ** SINF2224 2013 assignment 2 Collart Gaetan info 19/4/2013
 ** SINF2224 2013 assignment 2 Claessens Simon info 19/4/2013
 **
 ** This program is free software; you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation; either version 2 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/**
 * A bounded priority Set (of integers), implemented as an ordered array where elements
 * are kept sorted.
 */
public class Set {

    private /*@ non_null */ int[] data; // the elements of the Set
    private  int size;                  // the number of elements in the Set


    /*@@
      @//CLASS INVARIANTS
      @ invariant size >= 0;
      @ invariant size <= data.length;
      @ invariant data.owner == this;
      @ // The array is ordered.
      @ invariant (\forall int i,j;
      @                    i >= 0 && i <= j && j < size;
      @                    data[i] <= data[j]);
      @// All elements are unique.
      @ invariant (\forall int i,j; 
      @                    i >= 0 && j >= 0 && i != j && i < size && j < size; 
      @                    data[i] != data[j]);
      @*/

    // Return a map of size n initialize to -1
    /*@@
      @ requires n >= 0;
      @ ensures \result != null;
      @ ensures \result.length == n;
      @ ensures \fresh(\result);
      @ ensures (\forall int i;
      @                  0<=i && i<n;
      @                  \result[i] == -1);
      @
      @ private static pure model int[] id(int n);
      @*/

    /**
     * Return an empty stack with a capacity of max elements (max >= 0).
     */
    /*@@
      @ requires max >= 0;
      @
      @ ensures size == 0;
      @ ensures data.length == max;
      @
      @ modifies size, data;
      @*/
    public Set(int max) {
        this.data = new int[max];
        /*@ set data.owner = this; */
        this.size = 0;
    }

    /**
     * Return the size of the Set.
     */
    /*@@
      @ ensures \result == size;
      @*/
    public /*@ pure @*/ int size() {
        return size;
    }

    /**
     * Return element at index {n}.
     */
    /*@@
      @ requires n >= 0;
      @ requires n < size;
      @
      @ ensures \result == data[n];
      @*/
    public /*@ pure @*/ int get(int n) {
        return data[n];
    }

    /**
     * Add {n} in the Set.  Returns the index where {n} was inserted.
     */

    /*@@
      @
      @ requires size < data.length;
      @
      @ ensures !\old(contains(n)) ==> size == \old(size) + 1;
      @ ensures \old(contains(n)) ==> size == \old(size);
      @ ensures \result == \old(indexOf(n));
      @ ensures get(\result) == n;
      @
      @ // If the element is inserted then all elements after n are 
      @ // just shifted on right.
      @ ensures !\old(contains(n)) ==> (\forall int b; 
      @                                         b >= \result && b < \old(size); 
      @                                         data[b+1] == \old(data[b]));
      @
      @ // All the elements in the previous Queue are in the new Queues.
      @ ensures (\forall int c; 
      @                  c > 0 && c < \result; 
      @                  data[c] == \old(data[c]));
      @
      @ // As we do in Queue.noDup() we comment this modifies.
      @ //modifies data[*], size;
      @*/
    public int enQueue(int n) {
        int i = indexOf(n);
        
        if(contains(n))
            return i;
        
        // shift elements one index up, from size-1 down to i.
        int j = size;
        
        while (j > i) {
            data[j] = data[j-1];
            j = j - 1;
        }
        data[i] = n;
        
        size = size + 1;
        
        return i;
    }


    /**
     * Remove and return highest (i.e. last) element in the Set.
     */
    /*@@
      @ requires size > 0;
      @
      @ ensures \result == \old(data[size-1]);
      @ ensures size == \old(size) - 1;
      @
      @ modifies size;
      @*/
    public int dequeue() {
        size = size - 1;
        return data[size];
    }

    /**
     * Returns the index of the first element greater or equal to {n} in the queue.
     * Returns the size of the queue if all elements are smaller than {n}.
     */
    /*@@
      @ ensures \result >= 0;
      @ ensures \result <= size;
      @
      @ // The return is the index where the element is greater
      @ // or equal than n.
      @ ensures \result != size ==> data[\result] >= n;
      @
      @ // All the elements before the result must be smaller than n.
      @ ensures (\forall int i;
      @                  i >= 0 && i < \result;
      @                  data[i] < n);
      @*/
    public /*@ pure @*/ int indexOf(int n) {
        int i = 0;
        while (i < size && data[i] < n) {
            i = i + 1;
        }
        return i;
    }

    /**
     * Returns {true} iff {n} is in the queue.
     */
    /*@@
      @ ensures \result <==> (indexOf(n) < size && data[indexOf(n)] == n);
      @*/
    public /*@ pure @*/ boolean contains(int n) {
        int i = indexOf(n);
        return (i < size && data[i] == n);
    }

}
