/*
** Copyright (C) 2008 José Vander Meulen <jose.vandermeulen@uclouvain.be>
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*
* Modified by Ch. Pecheur, Mar 2013: remove model fields, express specs wrt. actual
* class fields directly. 
*/

// A stack with a bounded capacity.
public class Stack {
    

    //*************************************************************************
    //FIELDS
    //*************************************************************************
    private int size;
    private final int[] integers;


    //*************************************************************************
    //CLASS INVARIANTS
    //@ invariant integers != null;
    //@ invariant size >= 0;
    //@ invariant size == integers.length;
    //*************************************************************************
    

    //*************************************************************************
    // Create an empty stack of size {max}.
    //*************************************************************************

    /*@@
      @ requires max >= 0;
      @ size == 0;
      @ integers.length == max;
      @ modifies intergers, size;
      @*/
    public Stack(final int max) {
        this.integers = new int[max];
        this.size = 0;
    }

    //*************************************************************************
    // Return the current top element of the stack without removing it from the stack.
    //*************************************************************************

    /*@@
      @ // requires size - n - 1 >= 0 && size - n - 1 < size;
      @ requires n <= size - 1 && n > -1;
      @ ensures \result == integers[size - n - 1];
      @*/
    public /* @ pure */ int peek(int n) {
        return integers[size - n - 1];
    }

    //*************************************************************************
    // Remove and Return the current top element of the stack.
    //*************************************************************************

    /*@@
      @ requires size > 0;
      @ ensures \result == \old(integers[size-1]);
      @ ensures size == \old(size) - 1;
      @ ensures integers[size] == 0;
      @ modifies size, integers[size - 1];
      @*/
    public int pop() {
        size = size - 1;
        int result = integers[size];
        integers[size] = 0;
        return result;
    }

    //*************************************************************************
    // Add a given element at the top of the stack.
    //*************************************************************************

    /*@@
      @ requires size < integers.length;
      @ ensures size == \old(size) + 1;
      @ ensures integers[size] == e;
      @ modifies size, integers[size - 1];
      @*/
    public void push(int e) {
        integers[size] = e;
        size = size + 1;
    }

    //*************************************************************************
    //Return the number of elements in the stack.
    //*************************************************************************

    /*@@
      @ ensure \result == size;
      @*/
    public int size() {
        return size;
    }


    //*************************************************************************
    //Return the number of free places in the stack.
    //*************************************************************************

    /*@@
      @ ensure \result == intergers.length - size;
      @*/
    public int free() {
        return integers.length - size;
    }
}
