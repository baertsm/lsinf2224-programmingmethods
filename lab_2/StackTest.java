public class StackTest {

    //@ ensures \result == x+y+z;
    public static int test(int x, int y, int z) {
        Stack stack = new Stack(10);
        stack.push(x);
        stack.push(y);
        stack.push(z);
        int a = stack.pop() + stack.pop() + stack.pop();
        return a;
    }
    
}