/*
** Copyright (C) 2010 José Vander Meulen <jose.vandermeulen@gmail.com>
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

public class BubbleVariant {


    // Return an identity map of size n
    /*@
      @ requires n >= 0;
      @ ensures \result != null;
      @ ensures \result.length == n;
      @ ensures \fresh(\result); // \fresh <=> a new array
      @ ensures (\forall int i; 0<=i && i<n; \result[i] == i);
      @
      @	private static pure model int[] id(int n);
      @*/


    /**
     * Inner iteration of a bubble sort.  Scan array a and swap
     * consecutive elements when the latter is smaller than the former.
     */
    /*@
      @ // perm[i] == the initial index of the value currently in a[i].
      @ private static ghost int[] perm; 

      @ requires a != null && a.length > 0;
      
      // the maximal element of a is in last position     
      @ ensures (\forall int j; j >= 0 && j < a.length; a[a.length - 1] >= a[j]);

      // the final elements of a are a permutation of the initial elements
      //   1) all final elements correspond to some initial element 
      @ ensures (\forall int j, k; j >= 0 && j < a.length && k == perm[j]; a[j] = \old(a[k]));
      
      //   2) different final elements correspond to different initial elements
      @ // ensures ...

      @*/
    public static void bubbleSP(/*@non_null@*/ final int[] a) {
        
        int i = 0;

        while (i != a.length - 1) {
            if (a[i + 1] < a[i]) {
                // @ ghost int temp_p;

                int temp = a[i];
                // @ set temp_p = perm[i];
                // @ set perm[i] = perm[i+1];
                a[i] = a[i + 1];

                // @ set perm[i+1] = temp_p;
                a[i + 1] = temp;

            }
            i = i + 1;
        }
    }
}
