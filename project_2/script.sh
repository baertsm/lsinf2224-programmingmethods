#!/bin/sh
# Script made for the assignment 2 of LSINF2224 (Programming method)
# Authors: (2014)
# - BAERTS Matthieu (SINF21MS)
# - VERMEYLEN Alex  (SINF21MS)

export PATH=/usr/lib/jvm/java-1.6.0/bin:/usr/local/ESCJava205:$PATH
export JAVA=/usr/lib/jvm/java-1.6.0/bin/java

for i in Queue Set Test; do
    echo -e "\n=== Launching $i ===\n"
    escj -loop 3 $i.java
done

