/*
 ** Copyright (C) 2008 José Vander Meulen <jose.vandermeulen@uclouvain.be>
 ** Copyright (C) 2010 Charles Pecheur <charles.pecheur@uclouvain.be>
 **
 ** This program is free software; you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation; either version 2 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 **
 ** Specifications made for the assignment 2 of LSINF2224 (Programming method)
 ** Authors: (2014)
 ** - BAERTS Matthieu (SINF21MS)
 ** - VERMEYLEN Alex  (SINF21MS)
 */

/**
 * A bounded priority queue (of integers), implemented as an ordered array where elements
 * are kept sorted.
 */
public class Queue {

    private /*@ non_null */ int[] data;    // the elements of the queue
    private  int size;      // the number of elements in the queue

    /*@@
      @ // Class invariant
      @     // Size between 0 and max
      @ invariant 0 <= size;
      @ invariant size <= data.length;
      @ invariant data.owner == this; // needed for our tests
      @
      @     // We have a priority queue
      @ invariant (\forall int i, j; i >= 0 && i <= j && j < size;
      @            data[i] <= data[j]);
     */

    /**
     * Return an empty stack with a capacity of max elements (max >= 0).
     */
    /*@
     @   // max is the size of the queue: has to be positive
     @ requires max >= 0;
     @
     @   // seems logic but needed for the tests
     @ ensures size == 0;
     @ ensures data.length == max;
     @
     @ modifies size, data;
     */
    public Queue(int max) {
        this.data = new int[max];
        /*@ set data.owner = this; */
        this.size = 0;
    }

    /**
     * Return the size of the queue.
     */
    /*@
     @   // seems logic but needed for the tests
     @ ensures \result == size;
     @
    */
    public /*@ pure */ int size() {
        return size;
    }

    /**
     * Return element at index {n}.
     */
    /*@
     @   // no need to check data and size (already done in class invariant)
     @   // n should be an existing index (i.e. an index where a value is
     @   // actually stored).
     @ requires 0 <= n && n < size;
     @ ensures \result == data[n]; // needed for the tests
     @
     */
    public /*@ pure */ int get(int n) {
        return data[n];
    }

    /**
     * Add {n} in the queue.  Returns the index where {n} was inserted.
     */
    /*@
     @   // We want to add a new item, size should be stickly smaller than
     @   // data.length (!= the class invariants)
     @ requires size < data.length; 
     @
     @   // Returns the index where {n} was inserted => indexOf(n)
     @ ensures \result == \old(indexOf(n));
     @ ensures get(\result) == n;
     @   // Size has been increased
     @ ensures size == \old(size) + 1;
     @
     @   // after n is inserted, we can be sure that all value stored at
     @   // smaller index are smaller or equal to n and all value stored at
     @   // bigger index are bigger or equal to n, because there's the
     @   // definition of the priority queue.
     @ ensures (\forall int k ; k >= 0 && k < \result ;
     @          data[k] == \old(data[k]));
     @ ensures (\forall int k ; k >= \result && k < \old(size) ;
     @          data[k+1] == \old(data[k]));
     @ 
     @ modifies size, data[indexOf(n)..size];
     */
    public int enqueue(int n) {
        int i = indexOf(n);

        // shift elements one index up, from size-1 down to i.
        int j = size;
        //@ loop_invariant j <= size && j >= i;
        //@ decreases j;
        while (j > i) {
            data[j] = data[j-1];
            j = j - 1;
        }

        data[i] = n;
        size = size + 1;
        return i;
    }


    /**
     * Remove and return highest (i.e. last) element in the queue.
     */
    /*@
     @   // We will remove one element, we should have at least one
     @ requires size > 0;
     @
     @   // return highest element
     @ ensures \result == \old(data[size-1]);
     @   // size--;
     @ ensures size == \old(size) - 1;
     @ ensures size >= 0;
     @
     @ modifies size;
     */
    public int dequeue() {
        size = size - 1;
        return data[size];
    }

    /**
     * Returns the index of the first element greater or equal to {n} in the queue.  
     * Returns the size of the queue if all elements are smaller than {n}.
     */
    /*@
     @
     @    // After the execution, we can be sure that :
     @    // 1) data[\result] >= n because it's one condition to exit the loop
     @    // 2) \result == size because it's another condition to exit the loop
     @    // 3) data[0] > n, which implies that we never enter the loop.
     @ ensures data[\result] >= n || \result == size || data[0] > n;
     @    // but at least, result is between 0 and size
     @ ensures 0 <= \result && \result <= size;
     @
     @    // All items before \result must be smaller than n
     @ ensures (\forall int j; j >= 0 && j < \result; data[j] < n);
     */
    public /*@ pure */ int indexOf(int n) {
        int i = 0;
        //@ loop_invariant i >= 0 && i <= size;
        //@ decreases size - i;
        while (i < size && data[i] < n) {
            i = i + 1;
        }
        return i;
    }

    /**
     * Returns {true} iff {n} is in the queue.  
     */
    /*@
     @
     @    // \result == true if data contains i AND the supposed index of n is
     @    //                 smaller than size.
     @    // \result == false otherwise.
     @ ensures \result ==> (\exists int i ; i >= 0 && i < data.length ;
     @                      data[i] == n);
     @ ensures \result <==> (indexOf(n) < size && data[indexOf(n)] == n);
     @
     */
    public /*@ pure */ boolean contains(int n) {
        int i = indexOf(n);
        return (i < size && data[i] == n);
    }

    /**
     * Just create a new array of int of length s init to 0.
     */
    /*@
     @ requires s >= 0;
     @
     @ ensures \result != null;
     @ ensures \result.length == s;
     @ ensures (\forall int i ; 0 <= i && i < s ; \result[i] == 0);
     @ ensures \fresh(\result); // a new array
     @
     @ private static pure model int[] oldIDs(int s);
     */

    /**
     * Removes duplicates in the list.
     */
    /*@
     @ private static ghost int[] oldData;
     @
     @   // After having removed dup, all items are different
     @   // Note: it seems we can't do (\forall (...) (\forall (...)))
     @ ensures (\forall int k, l ;
     @          0 <= k && k < size &&
     @          0 <= l && l < size &&
     @          k != l ;
     @          data[k] != data[l]);
     @   // We didn't remove items from the previous data array
     @ ensures (\forall int k ; 0 <= k && k < size ;
     @          (\exists int l ; 0 <= l && l <= \old(size) ;
     @           data[k] == \old(data[l])));
     @   // All items are available in the previous queue too
     @ ensures (\forall int k ; 0 <= k && k < \old(size) ;
     @         data[oldData[k]] == \old(data[k]));
     @   // it seems we can't use 'modifies' due to the oldData, maybe because
     @   // the pointer has been modified...
     @ // modifies size, data[0..size], oldData, oldData[0..oldData.length];
      */
    public void noDup() {
        //@ set oldData = oldIDs(size);
        int i = 0; // current element in the old list
        int j = 0; // next spot to fill in the new list
        
        //@ loop_invariant i <= size;
        //@ decreases size - j;
        while (i < size) {
            data[j] = data[i];
            //@ assume j <= i;
            //@ set oldData[i] = j; // we copy the previous id
            i = i + 1;
            //@ loop_invariant i <= size;
            //@ decreases size - i;
            while (i < size && data[i] == data[j]) {
                //@ set oldData[i] = j;
                i = i + 1;
            }
            j = j + 1;
        }
        size = j;
    }

}
