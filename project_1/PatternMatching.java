/*
** Copyright (C) 2008 José Vander Meulen <jose.vandermeulen@uclouvain.be>, 
**                    Charles Pecheur <charles.pecheur@uclouvain.be>
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
** Specifications authors:
** - BAERTS Matthieu
** - VERMEYLEN Alex
**
*/

public class PatternMatching {

    /**
     * Returns true iff p occurs as a substring in t starting at index k.
     */
    /*@
      @   // p and t can't be null. If they were, the use of p.length
      @   //  (or t.length) would cause a "NullPointerException".
      @ requires p != null;
      @ requires t != null;
      @   // k is used in t[k + i]: it means that k + i < t.length and
      @   //  i is [0; p.length[ (the max value is p.length - 1)
      @ requires 0 <= k && k + p.length - 1 < t.length;
      @
      @   // Means that result is true iff:
      @   // - k is such as, starting at index k, there's enough room in the
      @   //   rest of t to contain p.
      @   // - For all indexes starting at k to k plus the length of p, each
      @   //   character in p is the same than the character in t at the index
      @   //   k plus the position of the character in p.
      @ ensures \result <==> ((k + p.length <= t.length) &&
      @     (\forall int i; i >= 0 && i < p.length; t[k + i] == p[i]));
      @
      @   // A pure method is one that doesn't have side effects. A non-pure
      @   //  method could result in code that works when compiled with
      @   //  assertion checking enabled but doesn't work when assertion
      @   //  checking is disabled.
      @ pure
      @*/
    private static boolean matches(int[] p, int[] t,  int k) {
        boolean result;
        if (k + p.length > t.length) {
            result = false;
        } else {
            int i = 0;
            boolean match = true;
            /*@
              @   // i can't be out of bounds during he loop execution.
              @   // Moreover, when we exit the loop, i = p.length.
              @ loop_invariant i >= 0 && i <= p.length;
              @   // k + i can't be out of bounds during the loop execution. In
              @   //  one case, k + i can be equal to t.length once we exit the
              @   //  loop. 
              @ loop_invariant k + i <= t.length;
              @   // 'match' stays equal to true until we found an index for
              @   //  which the 'i'th character in 'p' isn't the same as the
              @   //  'i'th character in the sub array 't' starting at index k
              @ loop_invariant match <==>
              @     (\forall int j; j >= 0 && j < i; p[j] == t[k + j]);
              @
              @   // p.length stays always at the same value while i is
              @   //  incremented at each turn of the loop. So, p.length - i
              @   //  decreases at each turn of the loop.
              @ decreases p.length - i;
              @*/
            while (i != p.length && match) {
                match = p[i] == t[k + i];
                i = i + 1;
            }
            result = match;
        }
        return result;
    }

    /**
     * Returns the smallest index i such that p is a substring of
     * t starting at i. Returns a negative number if p is not
     * a substring of t.
     */
    /*@
      @   // p and t can't be null. If they were, the use of p.length
      @   //  (or t.length) would cause a "NullPointerException".
      @ requires p != null && t != null;
      @   // no need to check the size of the arrays, no crash possible
      @
      @   // If the result is not negative, it means that an occurence of the
      @   //  substring p was found in t.
      @   // Moreover, this occurence, being the first one, is necessarily at
      @   //  the position "result".
      @ ensures \result >= 0 ==> matches(p, t, \result);
      @   // We see with the preceding post-condition that if the result isn't
      @   //  negative then an occurence of the substring was found beginning
      @   // at the position "result".
      @   // Then, It means that all positions before the result were not
      @   //  successful ('matches' returns "false"). 
      @ ensures \result >= 0 ==>
      @     (\forall int i; i >= 0 && i < \result; ! matches(p, t, i));
      @   // If the result is negative then no occurence of the substring p
      @   //  was found in t.
      @   // Then, for all indexes in t, 'matches' returned "false".
      @ ensures \result <  0 ==> (\forall int i; i >= 0 &&
      @     i <= t.length - p.length; ! matches(p, t, i));
      @
      @ pure
      @*/
    public static int find(int[] p, int[] t) {
        int i = 0;
        /*@
          @   // i can't be out of bounds during he loop execution. Moreover,
          @   //  when we exit the loop, i = t.length + p.length + 1
          @   //  (except if t.lenght - p.length < 0, it's not checked in Pre)
          @ loop_invariant i >= 0 && (i <= t.length - p.length + 1 || i == 0);
          @   // The method stops when a match is found. So, we can be sure
          @   //  that for all indexes smaller than the one which matches, the
          @   //  method 'matches' returns "false".
          @ loop_invariant matches(p, t, i) ==> (\forall int j; j >= 0 &&
          @     j < i;  ! matches(p, t, j));
          @   // For each index i considered in the loop, 2 cases are possible:
          @   //  1. i matched: the method 'matches' returned "true".
          @   //  2. i didn't match: i & all the previous indexes didn't match.
          @ loop_invariant matches(p, t, i) ||  (\forall int j; j >= 0 &&
          @     j <= i; ! matches(p, t, j));
          @
          @   // t.length and p.length are constant. i increases at each turn
          @   //  of the loop. So, 't.length - p.length - i' will decreases at
          @   // each turn of the loop.
          @   // The '+1' assures that the variant will never be negative
          @   //  because i = t.length - p.length + 1 when we exit the loop if
          @   //  'matches(p, t, i)' never returs "true".
          @ decreases t.length - p.length + 1 - i;
          @*/
        while (i <= t.length - p.length) {
            if (matches(p, t, i)) {
                return i;
            }
            i = i + 1;
        }
        return -1;
    }

    /**
     * Returns the smallest index i such that p is a substring of t starting
     * at i where n <= i.
     * Returns a negative number if p is not a substring of the substring of
     * t[n] to t[t.length-1].
     */
    /*@
      @   // p and t can't be null. If they were, the use of p.length
      @   //  (or t.length) would cause a "NullPointerException".
      @ requires p != null && t != null;
      @   // n should be greater than 0 because we begin to search for an
      @   //  occurence of the substring p at the nth position in t. If n was
      @   //  smaller than 0, we would begin to search out of bound, what
      @   //  would cause an exception.
      @ requires n >= 0;
      @   // if n > t.length - p.length, there is no crash, just return -1;
      @
      @   // If the result is not negative, it means that an occurence of the
      @   //  substring p was found in t.
      @   // Moreover, this occurence, being the first one, is necessarily at
      @   //  the position "result".
      @ ensures \result >= 0 ==> matches(p, t, \result);
      @   // We see with the previous post-condition that if the result isn't
      @   //  negative then an occurence of the substring was found beginning
      @   //  at the position "result".
      @   // Then, it means that all positions between n and result were not
      @   //  successful ('matches' returns "false").
      @ ensures \result >= 0 ==> (\forall int i; i >= n &&
      @     i < \result; ! matches(p, t, i));
      @   // If the result is negative then no occurence of the substring p
      @   //  was found in t.
      @   // Then, for all the positions in t starting at n, matches returned
      @   //  "false".
      @ ensures \result <  0 ==> (\forall int i; i >= n &&
      @     i <= t.length - p.length; ! matches(p, t, i));
      @
      @ pure
      @*/
    public static int find(int[] p, int[] t, int n) {
        int j = n; // needed for JVM to distinct the parameter and variable n
        /*@
          @   // According to the Pre, j is positive and there is no maximum
          @ loop_invariant j >= 0;
          @   // The method stops when a match is found. So, we can be sure
          @   //  that for all indexes smaller than the one which matches
          @   //  (and >= n), the method 'matches' returns "false".
          @ loop_invariant matches(p, t, j) ==> (\forall int i; i >= n &&
          @     i <  j; ! matches(p, t, i));
          @   // For each index i considered in the loop, 2 cases are possible:
          @   //  1. i matched: the method 'matches' returned "true".
          @   //  2. i didn't match: i & all the previous indexes didn't match.
          @ loop_invariant matches(p, t, j) ||  (\forall int i; i >= n &&
          @     i <= j; ! matches(p, t, i));
          @
          @   // t.length and p.length are constant. j increases at each turn
          @   //  of the loop. So, t.length - p.length - i will decreases at
          @   //  each turn of the loop. The '+ 1' assures that the variant
          @   //  will never be negative because j = t.length - p.length + 1
          @   //  when we exit the loop and 'matches(p, t, j)' is never "true".
          @ decreasing t.length - p.length - j + 1;
          @*/
        while (j <= t.length - p.length) {
            if (matches(p, t, j)) {
                return j;
            }
            j = j + 1;
        }
        return -1;
   }

    /**
     * Returns the greatest index i such that p is a substring of t starting at i.
     * Returns a negative number if p is not a substring of t.
     */
    /*@
      @   // p and t can't be null. If they were, p.length or t.length
      @   //  (used in the first line of the method) would cause a
      @   //  "NullPointerException").
      @ requires p != null && t != null;
      @   // no need to check the size of the arrays: no crash possible here.
      @
      @   // If the result is not negative, it means that an occurence of the
      @   //  substring p was found in t.
      @   // Moreover, this occurence, being the last one, is necessarily at
      @   //  the position "result".
      @ ensures \result >= 0 ==> matches(p, t, \result);
      @   // It also means that all indexes after this result used with the
      @   //  'matches' method always return "false"
      @ ensures \result >= 0 ==> (\forall int j; j >  \result &&
      @     j <= t.length - p.length; ! matches(p, t, j));
      @   // If the result is negative, it means that there is no match at all.
      @ ensures \result <  0 ==> (\forall int j; j >= 0 &&
      @     j <= t.length - p.length; ! matches(p, t, j));
      @
      @ pure
      @*/
    public static int findLast(int[] p, int[] t) {
        int i = 0;
        int r = -1;
        /*@
          @   // i can't be out of bounds during he loop execution. Moreover,
          @   //  when we exit the loop, i = t.lenght - p.length + 1
          @   //  (except if t.lenght - p.length < 0, it's not checked in Pre)
          @ loop_invariant i >= 0 && (i <= t.length - p.length + 1 || i == 0);
          @   // r smaller than zero means that no match has been found so far.
          @   //  then, we can be sure that, for all index lower the current
          @   //  one, no match has been found.
          @ loop_invariant r <  0 ==> (\forall int j; j >= 0 &&
          @     j < i; ! matches(p, t, j));
          @   // r not negative means that at least one match has been found
          @   //  so far (at index r). So, we can be sure that, for all indexes
          @   //  between r (the last match found) and the current index, there
          @   //  are no other match found.
          @ loop_invariant r >= 0 ==> (\forall int j; j >  r &&
          @     j < i; ! matches(p, t, j));
          @   // r not negative means that at least one match has been found so
          @   //  far (at index r). So, we can be sure that the matches method
          @   //  returned "true" for the index r.
          @ loop_invariant r >= 0 ==> matches(p, t, r);
          @
          @   // t.length and p.length are constant. i increases at each turn
          @   //  of the loop. So, 't.length - p.length - i' will decreases at
          @   //  each turn of the loop.
          @   // The '+1' assures that the variant will never be negative.
          @ decreasing t.length - p.length - i + 1;
          @*/
        while (i <= t.length - p.length) {
            if (matches(p, t, i)) {
                r = i;
            }
            i = i + 1;
        }
        return r;
    } 
}
