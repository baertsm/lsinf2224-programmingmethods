#!/bin/bash

javac bank/*.java || exit 1

LOG="log/test_"
LOGTMP="log/test_tmp_"

mkdir -p log
rm -f ${LOG}*.log
rm -f ${LOGTMP}*.log

MINITT=3
MAXITT=10

echo "Part 0"
sed -i "s/concurrentTransactions(3, 3, 20, 10)/concurrentTransactions(2, 2, 2, 2)/g" bank/TestHarness.java
timeout 10m time ./script.sh >> ${LOGTMP}0.log
sed -i "s/concurrentTransactions(2, 2, 2, 2)/concurrentTransactions(3, 3, 20, 10)/g" bank/TestHarness.java
grep time ${LOGTMP}0.log -A 7 > ${LOG}0.log

echo "Part 1"
for i in `seq $MINITT $MAXITT`; do
	sed -i "s/concurrentTransactions(3, 3, 20, 10)/concurrentTransactions($i, 2, 2, 2)/g" bank/TestHarness.java
	timeout 10m time ./script.sh >> ${LOGTMP}1.log
	echo -e "\n\n===================================================\n\n" >> ${LOGTMP}1.log
	echo $i
	sed -i "s/concurrentTransactions($i, 2, 2, 2)/concurrentTransactions(3, 3, 20, 10)/g" bank/TestHarness.java
done
grep time ${LOGTMP}1.log -A 7 > ${LOG}1.log

echo "Part 2"
for i in `seq $MINITT $MAXITT`; do
	sed -i "s/concurrentTransactions(3, 3, 20, 10)/concurrentTransactions(2, $i, 2, 2)/g" bank/TestHarness.java
	timeout 10m time ./script.sh >> ${LOGTMP}2.log
	echo -e "\n\n===================================================\n\n" >> ${LOGTMP}2.log
	echo $i
	sed -i "s/concurrentTransactions(2, $i, 2, 2)/concurrentTransactions(3, 3, 20, 10)/g" bank/TestHarness.java
done
grep time ${LOGTMP}2.log -A 7 > ${LOG}2.log

echo "Part 3"
for i in `seq $MINITT $MAXITT`; do
	sed -i "s/concurrentTransactions(3, 3, 20, 10)/concurrentTransactions(2, 2, $i, 2)/g" bank/TestHarness.java
	timeout 10m time ./script.sh >> ${LOGTMP}3.log
	echo -e "\n\n===================================================\n\n" >> ${LOGTMP}3.log
	echo $i
	sed -i "s/concurrentTransactions(2, 2, $i, 2)/concurrentTransactions(3, 3, 20, 10)/g" bank/TestHarness.java
done
grep time ${LOGTMP}3.log -A 7 > ${LOG}3.log

echo "Part 4"
for i in `seq $MINITT $MAXITT`; do
	sed -i "s/concurrentTransactions(3, 3, 20, 10)/concurrentTransactions(2, 2, 2, $i)/g" bank/TestHarness.java
	timeout 10m time ./script.sh >> ${LOGTMP}4.log
	echo -e "\n\n===================================================\n\n" >> ${LOGTMP}4.log
	echo $i
	sed -i "s/concurrentTransactions(2, 2, 2, $i)/concurrentTransactions(3, 3, 20, 10)/g" bank/TestHarness.java
done
grep time ${LOGTMP}4.log -A 7 > ${LOG}4.log
rm -f ${LOGTMP}*.log
