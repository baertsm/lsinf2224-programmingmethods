#!/bin/bash

export PATH=/usr/lib/jvm/java-1.6.0/bin:$PATH
export JAVA=/usr/lib/jvm/java-1.6.0/bin/java

javac bank/*.java || exit 1

mkdir -p log

echo "Part 1"
for i in `seq 3 5`; do
	sed -i "s/concurrentTransactions(3, 3, 20, 10)/concurrentTransactions($i, 2, 2, 2)/g" bank/Account.java
	./script.sh >> log/test_1.log
	echo -e "\n\n===================================================\n\n" >> log/test_1.log
	echo $i
	sed -i "s/concurrentTransactions($i, 2, 2, 2)/concurrentTransactions(3, 3, 20, 10)/g" bank/Account.java
done

echo "Part 2"
for i in `seq 3 5`; do
	sed -i "s/concurrentTransactions(3, 3, 20, 10)/concurrentTransactions(2, $i, 2, 2)/g" bank/Account.java
	./script.sh >> log/test_2.log
	echo -e "\n\n===================================================\n\n" >> log/test_2.log
	echo $i
	sed -i "s/concurrentTransactions(2, $i, 2, 2)/concurrentTransactions(3, 3, 20, 10)/g" bank/Account.java
done

echo "Part 3"
for i in `seq 3 5`; do
	sed -i "s/concurrentTransactions(3, 3, 20, 10)/concurrentTransactions(2, 2, $i, 2)/g" bank/Account.java
	./script.sh >> log/test_3.log
	echo -e "\n\n===================================================\n\n" >> log/test_3.log
	echo $i
	sed -i "s/concurrentTransactions(2, 2, $i, 2)/concurrentTransactions(3, 3, 20, 10)/g" bank/Account.java
done

echo "Part 4"
for i in `seq 3 5`; do
	sed -i "s/concurrentTransactions(3, 3, 20, 10)/concurrentTransactions(2, 2, 2, $i)/g" bank/Account.java
	./script.sh >> log/test_4.log
	echo -e "\n\n===================================================\n\n" >> log/test_4.log
	echo $i
	sed -i "s/concurrentTransactions(2, 2, 2, $i)/concurrentTransactions(3, 3, 20, 10)/g" bank/Account.java
done
