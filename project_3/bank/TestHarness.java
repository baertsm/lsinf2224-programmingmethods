/*
** Copyright (C) 2008 José Vander Meulen <jose.vandermeulen@uclouvain.be>
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
package bank;

import java.util.Random;

public class TestHarness {

    private static final Random random = new Random();

    /**
     * A test run with "nbrOfTrans" concurrent transfers between
     * randomly selected accounts among "nbrOfAccount".  Initial account balances
     * are randomly picked in "1..maxInitAmount" and transfer amounts are randomly
     * picked in "1..maxTransferAmount".
     */
    public static void concurrentTransactions(
            final int nbrOfTransfer,
            final int nbrOfAccount,
            final int maxInitAmount,
            final int maxTransferAmount) {

        // avoid surprises
        if (nbrOfAccount < 2 || nbrOfTransfer < 1 || maxInitAmount < 2
            || maxTransferAmount < 2)
            return;

        final Account[] accounts = new Account[nbrOfAccount];

        int incInitAmount = 0;
        int minInitAmout = 0;
        int i = 0;
        while (i != accounts.length) {
            incInitAmount = random.nextInt(maxInitAmount - minInitAmout);
            minInitAmout += incInitAmount;
            accounts[i] = new Account(minInitAmout + 1);
            i = i + 1;
        }

        // create a separated thread to randomly create accounts
        new Thread() {
            @Override
            public void run() {
                int incAccount = 0;
                int accNb = 0;
                int i = 0;
                while (i != nbrOfTransfer) {
                    // random account
                    incAccount = random.nextInt(accounts.length - accNb);
                    accNb += incAccount;
                    final int accNbThread = accNb;
                    final int thread = i;
                    new Thread() {
                        @Override
                        public void run() {
                            System.out.printf("Thread %d: begin%n", thread);
                            final Account a1 = accounts[accNbThread];
                            // transfer to another random account
                            int nextAccount = random.nextInt(accounts.length -
                                                  accNbThread) + accNbThread;
                            final Account a2 = accounts[nextAccount];

                            // A random account
                            final int amount = random.nextInt(maxTransferAmount) + 1;

                            System.out.printf("Thread %d:  #%d = %d, #%d = %d%n", thread, a1.id(), a1.balance(), a2.id(), a2.balance());
                            System.out.printf("Thread %d:  %d from #%d to #%d%n", thread, amount, a1.id(), a2.id());
                            final int result = a1.transferTo(a2, amount);
                            System.out.printf("Thread %d:  #%d = %d, #%d = %d%n", thread, a1.id(), a1.balance(), a2.id(), a2.balance());
                            System.out.printf("Thread %d: end%n", thread);
                        }
                    }.start();
                    i = i + 1;
                }
            }
        }.start();

        // merge two account
        new Thread() {
            @Override
            public void run() {
                int acc1 = random.nextInt(nbrOfAccount);
                int acc2;
                do {
                    acc2 = random.nextInt(nbrOfAccount);
                } while (acc1 != acc2);
                accounts[acc1].merge(accounts[acc2]);
            }
        }.start();

        // Assert thread: check balance is positive
        new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < accounts.length; i++)
                    assert accounts[i].balance() >= 0;
            }
        }.start();
    }

    /**
     * Start test with chosen parameters.
     */
    public static void main(String[] args) {
        concurrentTransactions(3, 3, 20, 10);
    }
}
