#!/bin/sh

export PATH=/usr/lib/jvm/java-1.6.0/bin:$PATH
export JAVA=/usr/lib/jvm/java-1.6.0/bin/java

javac philosophe/*.java || exit 1
jpf philosophe.jpf
