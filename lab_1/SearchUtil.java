/*
** Copyright (C) 2010 Jos� Vander Meulen <jose.vandermeulen@uclouvain.be>, 
**                    Charles Pecheur <charles.pecheur@uclouvain.be>
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

public class SearchUtil {

	//@ requires a != null;
	//@ ensures \result <==> ( \exists int i; i >= 0 && i < a.length ; a[i] == element );
	private static boolean search(int[] a, int element) {
		int i = 0;
		// escj -LoopSafe SearchUtil.java
		//@ loop_invariant i >= 0 && i <= a.length;
		//@ loop_invariant (\forall int j; 0 <= j && j < i; a[j] != element);
		//@ decreases a.length - i;
		while(i != a.length && a[i] != element) {
			i = i + 1;
		}
		
		return i != a.length;
	}

	//@ requires elements != null;
	//@ requires a != null;
	//@ ensures \result <==> ( \exists int i ; i >= 0 && i < elements.length; search(a, elements[i]) );
	private static boolean search(int[] a, int[] elements) {
		int i = 0;
		boolean result = false;
		//@ loop_invariant i >= 0 && i <= elements.length;
		//@ loop_invariant result <==> (\exists int j; j >= 0 && j < i; search(a, elements[j]));
		//@ decreases elements.length - i;
		while(i != elements.length && !result) {
			result = search(a, elements[i]);
			i++;
		}
		return result;
	}
}
